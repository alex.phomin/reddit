import { NgModule } from '@angular/core';
import {BrowserModule, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RedditData } from '../providers/reddit-data';
import { AllDataProvider } from './../providers/all-data';
import { IonicStorageModule } from '@ionic/storage';
import { IonicSwipeAllModule } from 'ionic-swipe-all';
import { IonicGestureConfig } from './hammer-config';


@NgModule({
  declarations: [
    AppComponent,
  ],
  entryComponents: [
    AppComponent,
  ],
  imports: [BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    IonicSwipeAllModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: IonicGestureConfig
    },
    RedditData,
    AllDataProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
