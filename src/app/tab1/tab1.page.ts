import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { AllDataProvider } from '../../providers/all-data';
import { IonicSwipeAllModule } from 'ionic-swipe-all';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {

  data: unknown;
  error: string;

  constructor(public allDataProvider: AllDataProvider) {}

    ionViewDidEnter() {
        this.allDataProvider.getAllPosts().then(
            data => {
                this.data = data;
            },
            err => {
                this.error = `An error occurred, the data could not be retrieved: Status: ${err.status}, Message: ${err.statusText}`;
            }
        );
  }

    onSwipeLeft($id) {
        console.log("onSwipeLeft", $id);
    }

    onSwipeRight($id) {
        this.allDataProvider.removePost($id).then(
            data => {
                console.log(this.data);
            }
        );
    }

}
