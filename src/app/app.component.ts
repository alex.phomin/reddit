import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { RedditData } from '../providers/reddit-data';
import { AllDataProvider } from '../providers/all-data';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public redditService: RedditData,
    public allDataProvider: AllDataProvider
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.getData();
    });
  }

  getData() {
    this.redditService.getRemoteData().subscribe(
        data => {
          this.allDataProvider.addPosts(data.data.children);
        },
        err => {
          this.allDataProvider.addPosts([]);
        }
    );
  }
}
