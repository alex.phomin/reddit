import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const STORAGE_KEY = 'starredPosts';

@Injectable()
export class StarredDataProvider {

    constructor(public storage: Storage) {}

    inStorage(postId) {
        return this.getAllPosts().then(result => {
            return result && result.indexOf(postId) !== -1;
        });
    }

    starredPost(postId) {
        return this.getAllPosts().then(result => {
            if (result) {
                result.push(postId);
                return this.storage.set(STORAGE_KEY, result);
            } else {
                return this.storage.set(STORAGE_KEY, [postId]);
            }
        });
    }

    unstarredPost(postId) {
        return this.getAllPosts().then(result => {
            if (result) {
                let index = result.indexOf(postId);
                result.splice(index, 1);
                return this.storage.set(STORAGE_KEY, result);
            }
        });
    }

    getAllPosts() {
        return this.storage.get(STORAGE_KEY);
    }
}