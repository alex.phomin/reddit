import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable()
export class RedditData {

    constructor(public http: HttpClient) {}

    getRemoteData(): Observable<object> {
        const dataUrl = 'https://cors-anywhere.herokuapp.com/https://www.reddit.com/r/angular.json';
        let data = this.http.get(dataUrl);
        return data;
    }

}