import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


const STORAGE_KEY = 'allPosts';

@Injectable()
export class AllDataProvider {

    constructor(public storage: Storage) {}

    addPosts(posts) {
        return this.storage.set(STORAGE_KEY, JSON.stringify(posts));
    }

    getAllPosts()  {
        return new Promise(resolve => {
            this.storage.get(STORAGE_KEY).then((data) => {
                resolve(JSON.parse(data));
            });
        })
    }

    removePost(postId) {
        return new Promise(resolve => {
            this.storage.get(STORAGE_KEY).then((data) => {
                let items = JSON.parse(data);
                items.splice(items.findIndex(item => item.id === postId), 1);
                resolve(this.addPosts(items));
            });
        });
    }
}